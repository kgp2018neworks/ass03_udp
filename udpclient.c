/* 
 * udpclient.c - A simple UDP client
 * usage: udpclient <host> <port>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUFSIZE 1024

/* 
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

//Checks for correct acknowledgemnet
int check_ack(int count, char buf[])
{
    char cbuf[10];
    int testc;
    sscanf(buf,"%s %04d", cbuf, &testc);
    if(count == testc)
    {
        printf("Acknowledgement for packet no. %d received\n", testc);
        return 1;
    }
    else return 0;
}

int main(int argc, char **argv) {
    int sockfd, portno, n;
    int serverlen;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    char buf[BUFSIZE];
    char rec_buf[BUFSIZE];
    char temp_buf[BUFSIZE-8];
    fd_set readfds;
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 000000;

    if (argc != 4) {
       fprintf(stderr,"usage: %s <hostname> <port> <filename>\n", argv[0]);
       exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);
    serverlen = sizeof(serveraddr);
    /* get a message from the user */
    //printf("Round 0 compn\n");
    bzero(buf, BUFSIZE);    
    FILE *filefd = fopen(argv[3], "rb");

    /*struct stat st;
    fstat(filefd, &st);
    int file_size = st.st_size;*/
    int file_size;
    fseek(filefd, 0, SEEK_END);
    file_size = ftell(filefd);
    rewind(filefd);
    int size = file_size,chunk=0;

    /*Calculation of total chunks*/
    while(size>0)
    {
        chunk++;
        size-=(BUFSIZE-8);
    }
    //printf("%d\n", file_size);
    
    /*Sending File descriptors*/
    sprintf(buf,"Filename: %s File_size: %d Total Chunks: %d", argv[3], file_size, chunk);
    printf("Filename: %s of filesize: %d being sent...\n", argv[3], file_size);
    
    //printf("buffer : %s\n", buf);
    //printf("Round 1/2 compn %d\n", chunk);
    
    n = sendto(sockfd, buf, strlen(buf), 0, &serveraddr, serverlen);
    if (n < 0) 
      error("ERROR in sendto");
    
    //printf("Round 2/3 compn\n");
    bzero(buf, BUFSIZE);
    
    n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, &serveraddr, &serverlen);
    if (n < 0) 
      error("ERROR in recvfrom");
    
    /*printf("Recvd ack: %s\n", buf);
    if(strcmp(buf, "ACK")){ 
        
        error("Wrong Acknowledgement");
    }*/
    
    
    //printf("Round 1 compn\n");
    int count = 0, read_return,i;
    bzero(buf, BUFSIZE);
    
    while (1) {
        count++;
        //printf("%d\n", count);
        bzero(temp_buf, BUFSIZE);
        bzero(buf, BUFSIZE);
        read_return = fread(temp_buf,1, BUFSIZE-8, filefd);
        if (read_return == -1) 
        {
            error("Couldn't read from file");
        }
        if (read_return == 0) 
        {
            //printf("done\n");
            fclose(filefd);
            break;
        }
        
        sprintf(buf, "%04d%04d%s", count, read_return, temp_buf);
        //printf(" %04d, %04d, %d, %d\n",  strlen(buf),  strlen(temp_buf), read_return, file_size);
        //printf("%s\n", temp_buf);
        printf("temp_buflen: %d\n", strlen(temp_buf));

        i = sendto(sockfd, buf, strlen(buf), 0, &serveraddr, serverlen);
        if (i == -1)
            error("Couldn't write to socket");
    
        FD_ZERO(&readfds);
        FD_SET(sockfd, &readfds);
        select(sockfd+1, &readfds, NULL, NULL, &tv);
        if(FD_ISSET(sockfd, &readfds))
        {
            bzero(rec_buf, BUFSIZE);
            n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, &serveraddr, &serverlen);
            if (n < 0) 
                error("ERROR in recvfrom");
            if(!check_ack(count, rec_buf))   
                error("Acknowledgement not received for same packet");
        }
        while(!FD_ISSET(sockfd, &readfds))
        {
            printf("wapis bheja\n");
            i = sendto(sockfd, buf, read_return, 0, &serveraddr, serverlen);
            if (i == -1)
                error("Couldn't write to socket");
            FD_ZERO(&readfds);
            FD_SET(sockfd, &readfds);
            select(sockfd+1, &readfds, NULL, NULL, &tv);
            if(FD_ISSET(sockfd, &readfds))
            {
                bzero(rec_buf, BUFSIZE);
                n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, &serveraddr, &serverlen);
                if (n < 0) 
                    error("ERROR in recvfrom");
                if(!check_ack(count, rec_buf))
                error("Acknowledgement not received for same packet");
            }
        }                     
    }
        printf("All files sent\n");
        bzero(rec_buf, BUFSIZE);
        n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, &serveraddr, &serverlen);
        if (n < 0) 
            error("ERROR in recvfrom");
        //printf("%s\n", rec_buf);
        if (strcmp(rec_buf, "ACK_FINAL"))
            error("Wrong Acknowledgement received 1");
        else
            printf("Closing connection request accepted\n");
        bzero(buf, BUFSIZE);
        sprintf(buf, "ACK");
        i = sendto(sockfd, buf, BUFSIZE, 0, &serveraddr, serverlen);
        if (i == -1)
                error("Couldn't write to socket");


}
